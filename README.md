# CPS1011 Assignment

This repository contains the source code for all the tasks which
had to be completed for the CPS1011 assignment. CPS1011 is a
compulsory unit at the University of Malta which has to be
taken by first year Computer Science and Mathematics undergraduates
during the first semester.

## The Structure of the Assignment

```
|- .clang-format
|- .gitignore
|- README.md
|- task1
|   |- .clang-format
|   |- CMakeLists.txt
|   `- src
|       |- .clang-format
|       |- question_a.c
|       |- question_a.h
|       |- question_a1_test.c
|       |- question_a2_test.c
|       |- question_a3_test.c
|       |- question_a4_test.c
|       |- question_a5_test.c
|       `- question_b.c
`- task2
    |- .clang-format
    |- CMakeLists.txt
    |- src
    |   |- .clang-format
    |   |- question_a.c
    |   |- question_a.h
    |   |- question_a_test.c
    |   |- question_b.c
    |   |- question_b.h
    |   `- question_b_test.c
    `- test_data
        |- test_data_2a.csv
        |- test_data_2a1.csv
        |- test_data_2b.csv
        `- test_data_2b1.csv
```

`.gitignore` is being used to make sure that all the files being generated by
`cmake` during testing are not pushed to the repository.

## Build Instructions

The minimum required version for `cmake` is `3.10`.

Starting from the root of the directory, execute the following commands.

```
$ cd task1
task1 $ mkdir build
task1/build $ cd build
task1/build $ cmake ..
task1/build $ make
task1/build $ cd ../../task2
task2 $ mkdir build
task2/build $ cd build
task2/build $ cmake ..
task2/build $ make
```

This generates 8 executables. Which are the following:

```
Task 1 (task1/build)    Task2 (task2/build)

./question_a1_test      ./question_a_test
./question_a2_test      ./question_b_test
./question_a3_test
./question_a4_test
./question_a5_test
./question_b
```

To execute the following binaries you must go into the build directory of the
respective task and type the following command where you must substitute
`{name_of_binary}` with one of the binaries described above.

```
{task1/task2}/build $ ./{name_of_binary}
```

E.g.

```
task1/build $ ./question_a1_test
task2/build $ ./question_b_test
```

## Testing

For task 2 ensure that example csv files are present in the directory of the
binaries. Ensure that the names are `test_data_2a.csv` and `test_data_2b.csv`.

Examples are provided in the `test_data` directory.
