cmake_minimum_required(VERSION 3.10)
project(assignment C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall")

add_executable(question_a_test src/question_a_test.c)
add_executable(question_b_test src/question_b_test.c)

add_library(question_a SHARED src/question_a.c)
add_library(question_b SHARED src/question_b.c)

target_link_libraries(question_a_test question_a)
target_link_libraries(question_b_test question_b)
