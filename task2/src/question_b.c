#include "question_b.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BLOCK_BUFF_SIZE 1024

#define SETERRNO(err, row, col, ret) \
    errnoDT = err;                   \
    erow = row;                      \
    ecol = col;                      \
    return ret;

typedef struct Block Block_t;

struct Block {
    char buff[BLOCK_BUFF_SIZE];
    Block_t *next;
};

size_t erow;
size_t ecol;
Error_t errnoDT;

/* for loading/unloading a file into memory */
static Block_t *fget_block(FILE *stream);
static void free_block(Block_t *block);

/* internal DataTable_t init function */
static DataTable_t *init(Field_t *fields, size_t size, size_t tot_rows);

/* for reading of double or string from data */
static int read_double(double *tmp_double, Block_t **ptr_block, size_t *pos, char *ch);
static int read_string(char **tmp_string, Block_t **ptr_block, size_t *pos, char *ch);

/* for printing of escaped characters */
static void printec(FILE *stream, char *string);
static void printen(FILE *stream, char *string);

static Block_t *fget_block(FILE *stream)  // return NULL to indicate error
{
    Block_t *head = (Block_t *)malloc(sizeof(Block_t));

    if (head == NULL) {
        return NULL;
    }

    head->next = NULL;

    Block_t *curr_block = head;  // current block
    size_t nread;                // number of objects read
    int eof;                     // return value of feof

    do {
        nread = fread(curr_block->buff, sizeof(char), BLOCK_BUFF_SIZE, stream);

        if ((eof = feof(stream)) != 0 && nread < BLOCK_BUFF_SIZE) {
            curr_block->buff[nread] = '\0';
        } else if (nread == BLOCK_BUFF_SIZE) {
            curr_block->next = (Block_t *)malloc(sizeof(Block_t));

            if (curr_block->next == NULL) {
                free_block(head);
                return NULL;
            }

            curr_block = curr_block->next;
            curr_block->next = NULL;
        }
    } while (eof == 0);

    return head;
}

static void free_block(Block_t *block)
{
    Block_t *curr_block = block;
    Block_t *prev_block = block;

    while (curr_block != NULL) {
        prev_block = curr_block;
        curr_block = curr_block->next;

        free(prev_block);
    }
}

static DataTable_t *init(Field_t *fields, size_t size, size_t tot_rows)
{
    DataTable_t *table = (DataTable_t *)calloc(1, sizeof(DataTable_t));

    if (table == NULL) {
        SETERRNO(EMEM, 0, 0, NULL)
    }

    table->max_cols = size;
    table->act_rows = 0;
    table->tot_rows = tot_rows;

    table->fields = (Field_t *)calloc(table->max_cols, sizeof(Field_t));

    if (table->fields == NULL) {
        deinitDT(table);
        SETERRNO(EMEM, 0, 0, NULL)
    }

    size_t string_len;

    for (size_t i = 0; i < table->max_cols; i++) {
        // assuming that labels are correct strings
        string_len = strlen(fields[i].label);

        table->fields[i].label = (char *)malloc(sizeof(char) * (string_len + 1));

        if (table->fields[i].label == NULL) {
            deinitDT(table);
            SETERRNO(EMEM, 0, 0, NULL)
        }

        // there is no need to check the return of strncpy because
        // table->fields[i].label is pointing to a location which is
        // string_len + 1 characters and fields[i].label is also
        // string_len + 1 characters.
        strncpy(table->fields[i].label, fields[i].label, string_len + 1);

        table->fields[i].type = fields[i].type;
    }

    table->cells = (Cell_t **)calloc(table->tot_rows, sizeof(Cell_t *));

    if (table->cells == NULL) {
        deinitDT(table);
        SETERRNO(EMEM, 0, 0, NULL)
    }

    for (size_t i = 0; i < table->tot_rows; i++) {
        table->cells[i] = (Cell_t *)calloc(sizeof(Cell_t), table->max_cols);

        if (table->cells[i] == NULL) {
            deinitDT(table);
            SETERRNO(EMEM, 0, 0, NULL)
        }
    }

    return table;
}

static int read_double(double *tmp_double, Block_t **ptr_block, size_t *pos, char *ch)
{
    size_t tmp_size = STRING_SIZE;
    size_t i = 0;

    char *tmp = (char *)malloc(sizeof(char) * tmp_size);

    if (tmp == NULL) {
        return 1;
    }

    char *offset;

    do {
        tmp[i++] = *ch;

        if (i == tmp_size) {
            tmp_size += STRING_SIZE;

            tmp = (char *)realloc(tmp, sizeof(char) * tmp_size);

            if (tmp == NULL) {
                return 1;
            }
        }

        (*pos)++;

        if (*pos == BLOCK_BUFF_SIZE) {
            *ptr_block = (*ptr_block)->next;
            *pos = 0;
        }

        *ch = (*ptr_block)->buff[*pos];
    } while (*ch != ',' && *ch != '\n' && *ch != '\0');

    (*pos)--;  // go back one because we are going to increment again in the caller
    tmp[i] = '\0';

    *tmp_double = strtod(tmp, &offset);

    if (*offset != ',' && *offset != '\n' && *offset != '\0') {
        free(tmp);
        return 2;
    }

    free(tmp);
    return 0;
}

static int read_string(char **tmp_string, Block_t **ptr_block, size_t *pos, char *ch)
{
    size_t tmp_size = STRING_SIZE;
    size_t i = 0;

    char *tmp = (char *)malloc(sizeof(char) * tmp_size);

    if (tmp == NULL) {
        return 1;
    }

    do {
        if (*ch == '\\') {
            (*pos)++;

            if (*pos == BLOCK_BUFF_SIZE) {
                *ptr_block = (*ptr_block)->next;
                *pos = 0;
            }

            *ch = (*ptr_block)->buff[*pos];

            switch (*ch) {
                case ',':
                    tmp[i++] = ',';
                    break;
                case '\\':
                    tmp[i++] = '\\';
                    break;
                default:
                    free(tmp);
                    return 2;
                    break;
            }
        } else {
            tmp[i++] = *ch;
        }

        if (i == tmp_size) {
            tmp_size += STRING_SIZE;

            tmp = (char *)realloc(tmp, sizeof(char) * tmp_size);

            if (tmp == NULL) {
                return 1;
            }
        }

        (*pos)++;

        if (*pos == BLOCK_BUFF_SIZE) {
            *ptr_block = (*ptr_block)->next;
            *pos = 0;
        }

        *ch = (*ptr_block)->buff[*pos];
    } while (*ch != ',' && *ch != '\n' && *ch != '\0');

    (*pos)--;
    tmp[i] = '\0';

    tmp = (char *)realloc(tmp, sizeof(char) * (strlen(tmp) + 1));

    if (tmp == NULL) {
        return 1;
    }

    *tmp_string = tmp;

    return 0;
}

static void printec(FILE *stream, char *string)
{
    for (size_t i = 0; i < strlen(string); i++) {
        if (string[i] == ',') {
            fputs("\\,", stream);
        } else if (string[i] == '\\') {
            fputs("\\\\", stream);
        } else {
            fputc(string[i], stream);
        }
    }

    fputc(',', stream);
}

static void printen(FILE *stream, char *string)
{
    for (size_t i = 0; i < strlen(string); i++) {
        if (string[i] == ',') {
            fputs("\\,", stream);
        } else if (string[i] == '\\') {
            fputs("\\\\", stream);
        } else {
            fputc(string[i], stream);
        }
    }

    fputc('\n', stream);
}

DataTable_t *initDT(Field_t *fields, size_t size)
{
    return init(fields, size, TABLE_ROWS_SIZE);
}

void deinitDT(DataTable_t *table)
{
    if (table != NULL) {
        if (table->cells != NULL) {
            for (size_t i = 0; i < table->tot_rows; i++) {
                if (table->cells[i] != NULL) {
                    for (size_t j = 0; j < table->max_cols; j++) {
                        if (table->fields[j].type == STRING &&
                            table->cells[i][j].str != NULL) {
                            free(table->cells[i][j].str);
                        }
                    }

                    free(table->cells[i]);
                }
            }

            free(table->cells);
        }

        if (table->fields != NULL) {
            for (size_t i = 0; i < table->max_cols; i++) {
                if (table->fields[i].label != NULL) {
                    free(table->fields[i].label);
                }
            }

            free(table->fields);
        }

        free(table);
    }
}

int loadDT(DataTable_t *table, char *file)
{
    if (table == NULL) {
        SETERRNO(ENULL, 0, 0, 1)
    }

    FILE *csv = fopen(file, "r");

    if (csv == NULL) {
        SETERRNO(EIO, 0, 0, 1)
    }

    Block_t *head = fget_block(csv);

    if (head == NULL) {
        SETERRNO(EMEM, 0, 0, 1)
    }

    Block_t *curr_block = head;

    size_t pos = 0;
    char ch = curr_block->buff[pos];

    size_t curr_col = 0;

    double tmp_double = 0;  // temporary double
    char *tmp_string;       // temporary string

    short empty = 0;  // to keep track if there is an empty cell
    short err = 0;    // to keep track of read_double and read_string errors

    while (ch != '\0') {
        switch (ch) {
                /* newline */
            case '\n':
                if (empty == 1) {
                    if (table->fields[curr_col].type == DOUBLE) {
                        table->cells[table->act_rows][curr_col].dbl = 0.0;
                    } else if (table->fields[curr_col].type == STRING) {
                        table->cells[table->act_rows][curr_col].str =
                            (char *)malloc(sizeof(char));
                        if (table->cells[table->act_rows][curr_col].str == NULL) {
                            free_block(head);
                            fclose(csv);
                            SETERRNO(EMEM, table->act_rows, curr_col, 1)
                        }
                        table->cells[table->act_rows][curr_col].str[0] = '\0';
                    } else {
                        free_block(head);
                        fclose(csv);
                        SETERRNO(ETYPE, table->act_rows, curr_col, 1)
                    }
                }

                while (curr_col < table->max_cols - 1) {
                    curr_col++;

                    if (table->fields[curr_col].type == DOUBLE) {
                        table->cells[table->act_rows][curr_col].dbl = 0.0;
                    } else if (table->fields[curr_col].type == STRING) {
                        table->cells[table->act_rows][curr_col].str =
                            (char *)malloc(sizeof(char));
                        if (table->cells[table->act_rows][curr_col].str == NULL) {
                            free_block(head);
                            fclose(csv);
                            SETERRNO(EMEM, table->act_rows, curr_col, 1)
                        }
                        table->cells[table->act_rows][curr_col].str[0] = '\0';
                    } else {
                        free_block(head);
                        fclose(csv);
                        SETERRNO(ETYPE, table->act_rows, curr_col, 1)
                    }
                }

                table->act_rows++;

                if (table->act_rows == table->tot_rows) {
                    table->tot_rows += TABLE_ROWS_SIZE;

                    table->cells = (Cell_t **)realloc(table->cells,
                                                      sizeof(Cell_t *) * table->tot_rows);

                    if (table->cells == NULL) {
                        free_block(head);
                        fclose(csv);
                        SETERRNO(EMEM, table->act_rows, curr_col, 1)
                    }

                    memset(&(table->cells[table->act_rows]), 0, TABLE_ROWS_SIZE);

                    for (size_t i = table->act_rows; i < table->tot_rows; i++) {
                        table->cells[i] =
                            (Cell_t *)calloc(sizeof(Cell_t), table->max_cols);

                        if (table->cells[i] == NULL) {
                            free_block(head);
                            fclose(csv);
                            SETERRNO(EMEM, table->act_rows, curr_col, 1)
                        }
                    }
                }

                curr_col = 0;

                empty = 1;
                break;

                /* comma */
            case ',':
                if (empty == 1) {
                    if (table->fields[curr_col].type == DOUBLE) {
                        table->cells[table->act_rows][curr_col].dbl = 0.0;
                    } else if (table->fields[curr_col].type == STRING) {
                        table->cells[table->act_rows][curr_col].str =
                            (char *)malloc(sizeof(char));
                        if (table->cells[table->act_rows][curr_col].str == NULL) {
                            free_block(head);
                            fclose(csv);
                            SETERRNO(EMEM, table->act_rows, curr_col, 1)
                        }
                        table->cells[table->act_rows][curr_col].str[0] = '\0';
                    } else {
                        free_block(head);
                        fclose(csv);
                        SETERRNO(ETYPE, table->act_rows, curr_col, 1)
                    }
                }

                curr_col++;

                if (curr_col >= table->max_cols) {
                    free_block(head);
                    fclose(csv);
                    SETERRNO(EBOUND, table->act_rows, curr_col, 1)
                }

                empty = 1;
                break;

                /* white space */
            case ' ':
                /* fallthrough */
            case '\t':
                /* do nothing */
                break;

                /* double */
            case '+':
                /* fallthrough */
            case '-':
                /* fallthrough */
            case '.':
                /* fallthrough */
            case '0':
                /* fallthrough */
            case '1':
                /* fallthrough */
            case '2':
                /* fallthrough */
            case '3':
                /* fallthrough */
            case '4':
                /* fallthrough */
            case '5':
                /* fallthrough */
            case '6':
                /* fallthrough */
            case '7':
                /* fallthrough */
            case '8':
                /* fallthrough */
            case '9':
                if ((err = read_double(&tmp_double, &curr_block, &pos, &ch)) == 2) {
                    free_block(head);
                    fclose(csv);
                    SETERRNO(EPARSE, table->act_rows, curr_col, 1)
                } else if (err == 1) {
                    free_block(head);
                    fclose(csv);
                    SETERRNO(EMEM, table->act_rows, curr_col, 1)
                }

                if (table->fields[curr_col].type == DOUBLE) {
                    table->cells[table->act_rows][curr_col].dbl = tmp_double;
                } else {
                    free_block(head);
                    fclose(csv);
                    SETERRNO(ETYPE, table->act_rows, curr_col, 1)
                }

                empty = 0;
                break;

                /* string */
            default:
                if ((err = read_string(&tmp_string, &curr_block, &pos, &ch)) == 2) {
                    free_block(head);
                    fclose(csv);
                    SETERRNO(EPARSE, table->act_rows, curr_col, 1)
                } else if (err == 1) {
                    free_block(head);
                    fclose(csv);
                    SETERRNO(EMEM, table->act_rows, curr_col, 1)
                }

                if (table->fields[curr_col].type == STRING) {
                    table->cells[table->act_rows][curr_col].str = tmp_string;
                } else {
                    free_block(head);
                    fclose(csv);
                    SETERRNO(ETYPE, table->act_rows, curr_col, 1)
                }

                empty = 0;
                break;
        }

        pos++;

        if (pos == BLOCK_BUFF_SIZE) {
            curr_block = curr_block->next;
            pos = 0;
        }

        ch = curr_block->buff[pos];
    }

    free_block(head);
    fclose(csv);

    return 0;
}

int exportDT(DataTable_t *table, char *file)
{
    if (table == NULL) {
        SETERRNO(ENULL, 0, 0, 1)
    }

    FILE *csv = fopen(file, "w");

    if (csv == NULL) {
        SETERRNO(EIO, 0, 0, 1)
    }

    for (size_t i = 0; i < table->act_rows; i++) {
        for (size_t j = 0; j < table->max_cols; j++) {
            if (table->fields[j].type == DOUBLE) {
                if (j == table->max_cols - 1) {
                    fprintf(csv, "%lf\n", table->cells[i][j].dbl);
                } else {
                    fprintf(csv, "%lf,", table->cells[i][j].dbl);
                }
            } else if (table->fields[j].type == STRING) {
                if (j == table->max_cols - 1) {
                    printen(csv, table->cells[i][j].str);
                } else {
                    printec(csv, table->cells[i][j].str);
                }
            }
        }
    }

    fclose(csv);

    return 0;
}

int showDT(DataTable_t *table)
{
    if (table == NULL) {
        SETERRNO(ENULL, 0, 0, 1)
    }

    // since snprintf() reads size - 1 we need 11 to get 10 meaningful characters
    int max = 10 + 1;
    size_t max_rows = 10;

    char output[max];
    char seps[max];
    char dots[max];

    for (size_t i = 0; i < max - 1; i++) {
        seps[i] = '-';
        dots[i] = '.';
    }

    seps[max - 1] = '\0';
    dots[max - 1] = '\0';

    for (size_t i = 0; i < table->max_cols; i++) {
        if (snprintf(output, max, "%s", table->fields[i].label) >= max) {
            output[max - 1 - 1] = '.';
            output[max - 1 - 2] = '.';
            output[max - 1 - 3] = '.';
        }

        if (i == table->max_cols - 1) {
            fprintf(stderr, "%*s\n", max - 1, output);
        } else {
            fprintf(stderr, "%*s\t", max - 1, output);
        }
    }

    for (size_t i = 0; i < table->max_cols; i++) {
        if (i == table->max_cols - 1) {
            fprintf(stderr, "%*s\n", max - 1, seps);
        } else {
            fprintf(stderr, "%*s\t", max - 1, seps);
        }
    }

    for (size_t i = 0; i < table->act_rows && i < max_rows; i++) {
        for (size_t j = 0; j < table->max_cols; j++) {
            if (table->fields[j].type == DOUBLE) {
                if (snprintf(output, max, "%.2lf", table->cells[i][j].dbl) >= max) {
                    // 1.(2 digit)e(+/-)(4 digits)
                    // 1 + 1 + 2 + 1 + 1 + 4 = 10
                    snprintf(output, max, "%.*e", max - 8 - 1, table->cells[i][j].dbl);
                }
            } else if (table->fields[j].type == STRING) {
                if (snprintf(output, max, "%s", table->cells[i][j].str) >= max) {
                    output[max - 1 - 1] = '.';
                    output[max - 1 - 2] = '.';
                    output[max - 1 - 3] = '.';
                }
            }

            if (j == table->max_cols - 1) {
                fprintf(stderr, "%*s\n", max - 1, output);
            } else {
                fprintf(stderr, "%*s\t", max - 1, output);
            }
        }
    }

    if (table->act_rows > max_rows) {
        for (size_t i = 0; i < table->max_cols; i++) {
            if (i == table->max_cols - 1) {
                fprintf(stderr, "%*s\n", max - 1, dots);
            } else {
                fprintf(stderr, "%*s\t", max - 1, dots);
            }
        }

        for (size_t j = 0; j < table->max_cols; j++) {
            if (table->fields[j].type == DOUBLE) {
                if (snprintf(output, max, "%.2lf",
                             table->cells[table->act_rows - 1][j].dbl) >= max) {
                    snprintf(output, max, "%.*e", max - 8 - 1,
                             table->cells[table->act_rows - 1][j].dbl);
                }
            } else if (table->fields[j].type == STRING) {
                if (snprintf(output, max, "%s",
                             table->cells[table->act_rows - 1][j].str) >= max) {
                    output[max - 1 - 1] = '.';
                    output[max - 1 - 2] = '.';
                    output[max - 1 - 3] = '.';
                }
            }

            if (j == table->max_cols - 1) {
                fprintf(stderr, "%*s\n", max - 1, output);
            } else {
                fprintf(stderr, "%*s\t", max - 1, output);
            }
        }
    }

    return 0;
}

DataTable_t *projectDT(DataTable_t *table, size_t m, size_t n, size_t x, size_t y)
{
    if (table == NULL) {
        SETERRNO(ENULL, 0, 0, NULL)
    }

    if (m > n || x > y) {
        SETERRNO(EORDER, 0, 0, NULL)
    }

    if (m >= table->max_cols || n >= table->max_cols) {
        SETERRNO(EBOUND, 0, 0, NULL)
    }

    if (x >= table->act_rows || y >= table->act_rows) {
        SETERRNO(EBOUND, 0, 0, NULL)
    }

    DataTable_t *projection = init(table->fields + m, n - m + 1, y - x + 1);

    if (projection == NULL) {
        SETERRNO(EMEM, 0, 0, NULL)
    }

    for (size_t i = x, r = 0; i <= y; i++, r++) {
        for (size_t j = m, c = 0; j <= n; j++, c++) {
            if (projection->fields[c].type == STRING) {
                projection->cells[r][c].str =
                    (char *)malloc(sizeof(char) * (strlen(table->cells[i][j].str) + 1));

                if (projection->cells[r][c].str == NULL) {
                    deinitDT(projection);
                    SETERRNO(EMEM, 0, 0, NULL)
                }

                strncpy(projection->cells[r][c].str, table->cells[i][j].str,
                        strlen(table->cells[i][j].str) + 1);
            } else if (projection->fields[c].type == DOUBLE) {
                projection->cells[r][c].dbl = table->cells[i][j].dbl;
            } else {
                deinitDT(projection);
                SETERRNO(ETYPE, i, j, NULL)
            }
        }
    }

    projection->act_rows = y - x + 1;

    return projection;
}

DataTable_t *mutateDT(DataTable_t *table, size_t col, Cell_t (*fpt)(Cell_t, Type_t))
{
    if (table == NULL) {
        SETERRNO(ENULL, 0, 0, NULL)
    }

    if (col >= table->max_cols) {
        SETERRNO(EBOUND, 0, 0, NULL)
    }

    for (size_t i = 0; i < table->act_rows; i++) {
        table->cells[i][col] = fpt(table->cells[i][col], table->fields[col].type);
    }

    return table;
}

#undef SETERRNO

#undef BLOCK_BUFF_SIZE
