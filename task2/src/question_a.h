#ifndef CPS1011_TASK2_QA_H_
#define CPS1011_TASK2_QA_H_

#include <stddef.h>

#define MAX_CELLS_PER_COL 1000
#define MAX_COLS 6
#define MAX_STRING_LEN 64
#define BUFF_SIZE 1024

extern size_t erow;
extern size_t ecol;

typedef enum {
    EIO = 0,          // error because: file operation
    ETYPE = 1,        // error because: invalid column data type
    EPARSE = 2,       // error because: parsing failure
    ELEN = 3,         // error because: maximum string length reached
    EESC = 4,         // error because: undefined escape sequence
    EBUFF = 5,        // error because: maximum input buffer size reached
    EMCOL = 6,        // error because: maximum column reached
    EMROW = 7,        // error because: maximum row reached
    EAROW = 8,        // error because: last active row reached
    EINEQUALITY = 9,  // error because: m > n or x > y in projectDT
    ENULL = 10,       // error because: NULL pointer
    EMEM = 11,        // error because: of allocation error memory
} Error_t;

extern Error_t errnoDT;

typedef enum {
    DOUBLE,
    STRING,
} Type_t;

typedef union {
    char string[MAX_STRING_LEN];
    double decimal;
} Cell_t;

typedef struct {
    char label[MAX_STRING_LEN];
    Type_t type;
} Field_t;

typedef struct {
    Cell_t cells[MAX_COLS][MAX_CELLS_PER_COL];  // [columns][rows]
    Field_t fields[MAX_COLS];
    size_t max_cols;  // maximum number of columns
    size_t act_rows;  // active number of rows
} DataTable_t;

/*
 * Operation: initialize an array with the required size.
 *
 * Pre-conditions: the size of the array 'fields' must be samller than or equal
 * to 6. Moreover, it requires enough memory.
 *
 * Parameters: 'fields' is an array, 'size' is the size of the array.
 *
 * Post-condition: the function will return a valid DataTable_t pointer if the
 * function is successful. Otherwise, it will return a NULL pointer.
 *
 * Possible errors: EMCOL, EMEM, ELEN
 */
DataTable_t *initDT(Field_t *fields, size_t size);

/*
 * Operation: frees any allocated table.
 *
 * Pre-condition: the table must have not been freed.
 *
 * Parameters: pointer to DataTable_t struct
 *
 * Post-condition: the memory allocated to the table is freed.
 */
void deinitDT(DataTable_t *table);

/*
 * Operation: parse a csv file and populate a DataTable_t struct
 *
 * Pre-condition: an allocated table and a csv file which follows the bounds
 * (e.g. maximum number of columns) and type specified during initialization.
 *
 * Parameters: an initialized table and a string which refers to a csv file
 * (e.g. "test_data_2a.csv").
 *
 * Post-condition: if the function successfully loads the csv file it will
 * return 0. Otherwise, it will return 1.
 *
 * Possible erros: ENULL, EIO, EBUFF, EMCOL, EMROW, ETYPE, EPARSE, EESC, ELEN
 *
 * N.B.: strings must be MAX_STRING_LEN - 1 characters long.
 */
int loadDT(DataTable_t *table, char *csv_file);

/*
 * Operation: the function serializes a DataTable_t into a .csv file.
 *
 * Pre-condition: the table needs to be initialized and the user must have the
 * rights to create a file. (Make sure the file does not already exist because
 * this function is destructive.)
 *
 * Parameters: an initialized table and a string which refers to a csv file
 * (e.g. "test_data_2a.csv").
 *
 * Post-condition: the function will return 0, if writing was successful.
 * Otherwise, the function will return 1.
 *
 * Possible errors: ENULL, EIO
 */
int exportDT(DataTable_t *table, char *csv_file);

/*
 * Operation: this function prints an initialized table in a tabular format.
 * The column width is 10 characters long.
 *
 * Pre-condition: table must be initialized.
 *
 * Parameters: an initialized table.
 *
 * Post-condition: the function will return 0 if successful. Otherwise, the
 * function will return 1.
 *
 * Possible errors: ENULL
 */
int showDT(DataTable_t *table);

/*
 * Operation: this function will return a pointer to a newly allocated subset
 * of an already existing table.
 *
 * Pre-condition: an initialized table, m <= n and x <= y.
 *
 * Parameters: an initialized table, m,n are use to select the columns. x,y are
 * used to select the rows.
 *
 * Post-condition: the function will return a valid pointer if successful.
 * Otherwise, it will return a NULL pointer.
 *
 * Possible errors: ENULL, EMEM, EINEQUALITY, EMCOL, EAROW, ELEN
 */
DataTable_t *projectDT(DataTable_t *table, size_t m, size_t n, size_t x, size_t y);

/*
 * Operation: this function will aid the user of the library to mutate the
 * contents in a specific column by providing their own function.
 *
 * Pre-condition: the table must be initialized, the column must within be a
 * valid column index within the table. The provided function must follow
 * specified signature.
 *
 * Parameters: an initialized table, a valid column index and a valid function
 * pointer.
 *
 * Post-condition: the function will return a valid pointer to the modified
 * table if successful. Otherwise, it will return a NULL pointer.
 *
 * Possible errors: ENULL, EMCOL
 *
 * N.B.: the user of the library must make sure that the functino provided is
 * safe e.g. checking that any string manipulations keep the string length
 * below MAX_STRING_LEN.
 */
DataTable_t *mutateDT(DataTable_t *table, size_t col, Cell_t (*fpt)(Cell_t, Type_t));

#endif /* CPS1011_TASK2_QA_H_ */
