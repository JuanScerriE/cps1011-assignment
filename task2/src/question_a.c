#include "question_a.h"

#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

size_t erow;
size_t ecol;
Error_t errnoDT;

static int fgetw(char *str, int nitems, FILE *stream);
static void printec(FILE *stream, char *string);
static void printen(FILE *stream, char *string);
static void seterrno(Error_t err, size_t row, size_t col);

DataTable_t *initDT(Field_t *fields, size_t size)
{
    if (size > MAX_COLS) {
        seterrno(EMCOL, 0, 0);
        return NULL;
    }

    DataTable_t *table = (DataTable_t *)malloc(sizeof(DataTable_t));

    if (table == NULL) {
        seterrno(EMEM, 0, 0);
        return NULL;
    }

    size_t string_len;

    for (size_t i = 0; i < size; i++) {
        // assuming that labels are correct strings
        string_len = strlen(fields[i].label);

        if (string_len >= MAX_STRING_LEN) {
            seterrno(ELEN, 0, 0);
            free(table);
            return NULL;
        }

        // strncpy return need not be checked and the string need not be
        // null terminated because the label must always be smaller than
        // MAX_STRING_LEN hence allowing at least one space for a \0
        strncpy(table->fields[i].label, fields[i].label, string_len + 1);
        table->fields[i].type = fields[i].type;
    }

    table->act_rows = 0;
    table->max_cols = size;

    return table;
}

void deinitDT(DataTable_t *table)
{
    if (table != NULL) {
        free(table);
    }
}

int loadDT(DataTable_t *table, char *file)
{
    if (table == NULL) {
        seterrno(ENULL, 0, 0);
        return 1;
    }

    FILE *csv = fopen(file, "r");

    if (csv == NULL) {
        seterrno(EIO, 0, 0);
        return 1;
    }

    char lbuff[BUFF_SIZE];  // line buffer

    char ch;
    size_t size;

    size_t col = 0;
    size_t row = 0;

    double dtmp = 0;            // temporary double
    char stmp[MAX_STRING_LEN];  // temporary string

    int eof;
    short empty = 0;

    do {
        eof = fgetw(lbuff, BUFF_SIZE, csv);

        // if strlen(lbuff) is 0, that implies
        // that an element could not fit
        // within the stipulated buffer size
        size = strlen(lbuff);

        if (size == 0) {
            seterrno(EBUFF, col, row);
            fclose(csv);
            return 1;
        }

        for (size_t i = 0; i < size; i++) {
            ch = lbuff[i];

            if (ch == '\n') {      // new row
                if (empty == 1) {  // dealing with empty cells
                    if (col >= table->max_cols) {
                        seterrno(EMCOL, 0, 0);
                        fclose(csv);
                        return 1;
                    }

                    if (table->fields[col].type == DOUBLE) {
                        table->cells[col][row].decimal = 0.0;
                    } else if (table->fields[col].type == STRING) {
                        table->cells[col][row].string[0] = '\0';
                    } else {
                        seterrno(ETYPE, col, row);
                        fclose(csv);
                        return 1;
                    }
                }

                while (col < table->max_cols - 1) {
                    col++;

                    if (table->fields[col].type == DOUBLE) {
                        table->cells[col][row].decimal = 0.0;
                    } else if (table->fields[col].type == STRING) {
                        table->cells[col][row].string[0] = '\0';
                    } else {
                        seterrno(ETYPE, col, row);
                        fclose(csv);
                        return 1;
                    }
                }

                row++;

                if (row >= MAX_CELLS_PER_COL) {
                    seterrno(EMROW, 0, 0);
                    fclose(csv);
                    return 1;
                }

                col = 0;
                empty = 1;
            } else if (ch == ',') {  // new column
                if (empty == 1) {    // dealing with empty cells
                    if (col >= table->max_cols) {
                        seterrno(EMCOL, 0, 0);
                        fclose(csv);
                        return 1;
                    }

                    if (table->fields[col].type == DOUBLE) {
                        table->cells[col][row].decimal = 0.0;
                    } else if (table->fields[col].type == STRING) {
                        table->cells[col][row].string[0] = '\0';
                    } else {
                        seterrno(ETYPE, col, row);
                        fclose(csv);
                        return 1;
                    }
                }

                col++;

                if (col >= table->max_cols) {
                    seterrno(EMCOL, col, row);
                    fclose(csv);
                    return 1;
                }

                empty = 1;
            } else if (ch == ' ' || ch == '\t') {  // only leading white space is ignored
                // do nothing
            } else if (ch == '+' || ch == '-' || ch == '.' || isdigit(ch)) {  // double
                char *offset = 0;
                dtmp = strtod(lbuff + i, &offset);
                // trailing white space will cause the function to return
                // an error for doubles
                if (*offset != ',' && *offset != '\n' && *offset != '\0') {
                    seterrno(EPARSE, col, row);
                    fclose(csv);
                    return 1;
                } else {
                    i = offset - lbuff - 1;  // -1 because of i++ in the for loop
                }

                if (table->fields[col].type == DOUBLE) {
                    table->cells[col][row].decimal = dtmp;
                } else {
                    seterrno(ETYPE, col, row);
                    fclose(csv);
                    return 1;
                }

                empty = 0;
            } else {  // string
                int offset = 0;

                // keep track of actual characters because of escaped characters
                int full_char_i = 0;

                // in this case you cannot just overlook spaces and tabs because they
                // might be part of the string
                while ((ch = lbuff[i + offset]) != ',' && ch != '\n' && ch != '\0' &&
                       full_char_i < MAX_STRING_LEN) {
                    if (ch == '\\') {
                        ch = lbuff[i + ++offset];

                        if (ch == ',') {
                            stmp[full_char_i++] = ',';
                        } else if (ch == '\\') {
                            stmp[full_char_i++] = '\\';
                        } else {
                            seterrno(EESC, col, row);
                            fclose(csv);
                            return 1;
                        }
                    } else {
                        stmp[full_char_i++] = ch;
                    }

                    offset++;
                }

                if (full_char_i >= MAX_STRING_LEN) {
                    seterrno(ELEN, col, row);
                    fclose(csv);
                    return 1;
                }

                i = i + offset - 1;
                stmp[full_char_i++] = '\0';

                if (table->fields[col].type == STRING) {
                    strncpy(table->cells[col][row].string, stmp, MAX_STRING_LEN);
                } else {
                    seterrno(ETYPE, col, row);
                    fclose(csv);
                    return 1;
                }

                empty = 0;
            }
        }
    } while (eof == 0);

    table->act_rows = row;

    fclose(csv);

    return 0;
}

int exportDT(DataTable_t *table, char *file)
{
    if (table == NULL) {
        seterrno(ENULL, 0, 0);
        return 1;
    }

    FILE *csv = fopen(file, "w");

    if (csv == NULL) {
        seterrno(EIO, 0, 0);
        return 1;
    }

    for (size_t i = 0; i < table->act_rows; i++) {
        for (size_t j = 0; j < table->max_cols; j++) {
            if (table->fields[j].type == DOUBLE) {
                if (j == table->max_cols - 1) {
                    fprintf(csv, "%lf\n", table->cells[j][i].decimal);
                } else {
                    fprintf(csv, "%lf,", table->cells[j][i].decimal);
                }
            } else if (table->fields[j].type == STRING) {
                if (j == table->max_cols - 1) {
                    printen(csv, table->cells[j][i].string);
                } else {
                    printec(csv, table->cells[j][i].string);
                }
            }
        }
    }

    fclose(csv);

    return 0;
}

int showDT(DataTable_t *table)
{
    if (table == NULL) {
        seterrno(ENULL, 0, 0);
        return 1;
    }

    // since snprintf() reads size - 1 we need 11 to get 10 meaningful characters
    int max = 10 + 1;
    size_t max_rows = 10;
    char output[max];

    char seps[max];
    char dots[max];

    for (size_t i = 0; i < max; i++) {
        seps[i] = '-';
        dots[i] = '.';
    }

    seps[max - 1] = '\0';
    dots[max - 1] = '\0';

    for (size_t i = 0; i < table->max_cols; i++) {
        if (snprintf(output, max, "%s", table->fields[i].label) >= max) {
            output[max - 1 - 1] = '.';
            output[max - 1 - 2] = '.';
            output[max - 1 - 3] = '.';
        }

        if (i == table->max_cols - 1) {
            fprintf(stderr, "%*s\n", max - 1, output);
        } else {
            fprintf(stderr, "%*s\t", max - 1, output);
        }
    }

    for (size_t i = 0; i < table->max_cols; i++) {
        if (i == table->max_cols - 1) {
            fprintf(stderr, "%*s\n", max - 1, seps);
        } else {
            fprintf(stderr, "%*s\t", max - 1, seps);
        }
    }

    for (size_t i = 0; i < table->act_rows && i < max_rows; i++) {
        for (size_t j = 0; j < table->max_cols; j++) {
            if (table->fields[j].type == DOUBLE) {
                if (snprintf(output, max, "%.2lf", table->cells[j][i].decimal) >= max) {
                    // 1.(2 digits)e(+/-)(4 digits)
                    // 1 + 1 + 2 + 1 + 1 + 4 = 10
                    // max - 8 - 1 <- (-1) because max = 10 + 1 to account for \0
                    snprintf(output, max, "%.*e", max - 8 - 1,
                             table->cells[j][i].decimal);
                }
            } else if (table->fields[j].type == STRING) {
                if (snprintf(output, max, "%s", table->cells[j][i].string) >= max) {
                    output[max - 1 - 1] = '.';
                    output[max - 1 - 2] = '.';
                    output[max - 1 - 3] = '.';
                }
            }

            if (j == table->max_cols - 1) {
                fprintf(stderr, "%*s\n", max - 1, output);
            } else {
                fprintf(stderr, "%*s\t", max - 1, output);
            }
        }
    }

    if (table->act_rows > max_rows) {
        for (size_t i = 0; i < table->max_cols; i++) {
            if (i == table->max_cols - 1) {
                fprintf(stderr, "%*s\n", max - 1, dots);
            } else {
                fprintf(stderr, "%*s\t", max - 1, dots);
            }
        }

        for (size_t j = 0; j < table->max_cols; j++) {
            if (table->fields[j].type == DOUBLE) {
                if (snprintf(output, max, "%.2lf",
                             table->cells[j][table->act_rows - 1].decimal) >= max) {
                    snprintf(output, max, "%.*e", max - 8 - 1,
                             table->cells[j][table->act_rows - 1].decimal);
                }
            } else if (table->fields[j].type == STRING) {
                if (snprintf(output, max, "%s",
                             table->cells[j][table->act_rows - 1].string) >= max) {
                    output[max - 1 - 1] = '.';
                    output[max - 1 - 2] = '.';
                    output[max - 1 - 3] = '.';
                }
            }

            if (j == table->max_cols - 1) {
                fprintf(stderr, "%*s\n", max - 1, output);
            } else {
                fprintf(stderr, "%*s\t", max - 1, output);
            }
        }
    }

    return 0;
}

DataTable_t *projectDT(DataTable_t *table, size_t m, size_t n, size_t x, size_t y)
{
    if (table == NULL) {
        seterrno(ENULL, 0, 0);
        return NULL;
    }

    if (m > n || x > y) {
        seterrno(EINEQUALITY, 0, 0);
        return NULL;
    }

    if (m >= table->max_cols || n >= table->max_cols) {
        seterrno(EMCOL, 0, 0);
        return NULL;
    }

    if (x >= table->act_rows || y >= table->act_rows) {
        seterrno(EAROW, 0, 0);
        return NULL;
    }

    DataTable_t *projection = initDT(table->fields + m, n - m + 1);

    if (projection == NULL) {
        seterrno(EMEM, 0, 0);
        return NULL;
    }

    for (size_t i = x, r = 0; i <= y; i++, r++) {
        for (size_t j = m, c = 0; j <= n; j++, c++) {
            // unions similar to structs, they are treated as variables
            // so the whole union is copied
            projection->cells[c][r] = table->cells[j][i];
        }
    }

    projection->max_cols = n - m + 1;
    projection->act_rows = y - x + 1;

    return projection;
}

DataTable_t *mutateDT(DataTable_t *table, size_t col, Cell_t (*fpt)(Cell_t, Type_t))
{
    if (table == NULL) {
        seterrno(ENULL, 0, 0);
        return NULL;
    }

    if (col >= table->max_cols) {
        seterrno(EMCOL, 0, 0);
        return NULL;
    }

    for (size_t i = 0; i < table->act_rows; i++) {
        table->cells[col][i] = fpt(table->cells[col][i], table->fields[col].type);
    }

    return table;
}

static int fgetw(char *str, int nitems, FILE *stream)
{
    size_t nread = fread(str, sizeof(char), nitems - 1, stream);
    str[nread] = '\0';

    int eof = feof(stream);

    if ((str[nread - 1] != ',' || str[nread - 1] != '\n' ||
         (str[nread - 2] == '\\' && str[nread - 1] == ',')) &&
        eof == 0) {
        long back = 1;

        while (back < nread &&
               (str[nread - (1 + back)] != ',' ||
                (str[nread - (2 + back)] == '\\' && str[nread - (1 + back)] == ',')) &&
               str[nread - (1 + back)] != '\n') {
            back++;
        }

        fseek(stream, -back, SEEK_CUR);
        str[nread - back] = '\0';
    }

    return eof;
}

static void printec(FILE *stream, char *string)
{
    for (size_t i = 0; i < strlen(string); i++) {
        if (string[i] == ',') {
            fputs("\\,", stream);
        } else if (string[i] == '\\') {
            fputs("\\\\", stream);
        } else {
            fputc(string[i], stream);
        }
    }

    fputc(',', stream);
}

static void printen(FILE *stream, char *string)
{
    for (size_t i = 0; i < strlen(string); i++) {
        if (string[i] == ',') {
            fputs("\\,", stream);
        } else if (string[i] == '\\') {
            fputs("\\\\", stream);
        } else {
            fputc(string[i], stream);
        }
    }

    fputc('\n', stream);
}

static void seterrno(Error_t err, size_t col, size_t row)
{
    errnoDT = err;
    ecol = col;
    erow = row;
}
