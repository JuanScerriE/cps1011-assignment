#include "question_b.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Cell_t test(Cell_t cell, Type_t type);

int main(void)
{
    Field_t labels[7] = {{"Price", DOUBLE},        {"Shipping", DOUBLE},
                         {"Tax", DOUBLE},          {"Brand", STRING},
                         {"Architecture", STRING}, {"Product", STRING},
                         {"Codename", STRING}};

    DataTable_t *table = initDT(labels, 7);

    if (table == NULL) {
        fprintf(stderr, "ERROR: memory allocation failed\n");
        exit(EXIT_FAILURE);
    }

    if (loadDT(table, "test_data_2b.csv")) {
        fprintf(stderr, "Error at column: %lu and row: %lu\n", ecol, erow);

        switch (errnoDT) {
            case EIO:
                fprintf(stderr, "ERROR: could not open file\n");
                break;
            case ENULL:
                fprintf(stderr, "ERROR: table is a NULL pointer\n");
                break;
            case EMEM:
                fprintf(stderr, "ERROR: memory allocation failed\n");
                break;
            case ETYPE:
                fprintf(stderr, "ERROR: column type does not match parsed type\n");
                break;
            case EPARSE:
                fprintf(stderr, "ERROR: type mismatch during parsing\n");
                break;
            case EBOUND:
                fprintf(stderr, "ERROR: exceeded some bound (e.g. maximum columns)\n");
                break;
            default:
                fprintf(stderr, "ERROR: failed due to some error\n");
                break;
        }

		deinitDT(table);

        exit(EXIT_FAILURE);
    }

    exportDT(table, "output_2b1.csv");  // further error checking can be done as above

    showDT(table);

    puts("");

    DataTable_t *table2 = projectDT(table, 3, 6, 10, 19);

    if (table2 == NULL) {
        fprintf(stderr, "ERROR: memory allocation failed\n");
        exit(EXIT_FAILURE);
    }

    mutateDT(table2, 1, test);

    showDT(table2);

    exportDT(table2, "output_2b2.csv");

    deinitDT(table);
    deinitDT(table2);

    return 0;
}

Cell_t test(Cell_t cell, Type_t type)
{
    if (type == DOUBLE) {
        cell.dbl = cell.dbl * 0.0;
    } else if (type == STRING) {
        for (size_t i = 0; i < strlen(cell.str); i++) {
            cell.str[i] = cell.str[i] + 2;
        }
    }

    return cell;
}
