#include "question_a.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Cell_t test(Cell_t cell, Type_t type);

int main(void)
{
    Field_t labels[MAX_COLS] = {{"Price", DOUBLE},        {"Shipping", DOUBLE},
                                {"Tax", DOUBLE},          {"Brand", STRING},
                                {"Architecture", STRING}, {"Product", STRING}};

    DataTable_t *table = initDT(labels, MAX_COLS);

    if (table == NULL) {
        fprintf(stderr, "ERROR: allocation failure/exceeded string length/size != 6\n");
        exit(EXIT_FAILURE);
    }

    if (loadDT(table, "test_data_2a.csv")) {
        fprintf(stderr, "Error at column: %lu and row: %lu\n", ecol, erow);

        switch (errnoDT) {
            case EMEM:
                fprintf(stderr, "ERROR: allocation failure\n");
                break;
            case ENULL:
                fprintf(stderr, "ERROR: NULL pointer\n");
                break;
            case EIO:
                fprintf(stderr, "ERROR: file is a NULL pointer\n");
                break;
            case ETYPE:
                fprintf(stderr, "ERROR: column type does not match parsed type\n");
                break;
            case EMCOL:
                fprintf(stderr, "ERROR: maximum column reached\n");
                break;
            case EBUFF:
                fprintf(stderr, "ERROR: input buffer is too small\n");
                break;
            case ELEN:
                fprintf(stderr, "ERROR: string length exceeded\n");
                break;
            case EPARSE:
                fprintf(stderr, "ERROR: type mismatch during parsing\n");
                break;
            default:
                fprintf(stderr, "ERROR: failed to load csv\n");
                break;
        }

        deinitDT(table);

        exit(EXIT_FAILURE);
    }

    showDT(table);

    DataTable_t *table2 = projectDT(table, 2, 4, 5, 7);

    if (table2 == NULL) {
        switch (errnoDT) {
            case EMEM:
                fprintf(stderr, "ERROR: allocation failure\n");
                break;
            case ENULL:
                fprintf(stderr, "ERROR: NULL pointer\n");
                break;
            case EINEQUALITY:
                fprintf(stderr, "ERROR: indexes are not ordered properly\n");
                break;
            case EMCOL:
                fprintf(stderr, "ERROR: greater than the number of columns\n");
                break;
            case EAROW:
                fprintf(stderr, "ERROR: greater than the number of active rows\n");
                break;
            default:
                fprintf(stderr, "ERROR: failed to project table\n");
                break;
        }

        deinitDT(table2);

        exit(EXIT_FAILURE);
    }

    puts("");

    showDT(table2);

    table2 = mutateDT(table2, 0, test);
    table = mutateDT(table, 3, test);

    if (exportDT(table, "output_2a1.csv")) {
        fprintf(stderr, "ERROR: could not open file\n");
        deinitDT(table);
        deinitDT(table2);
        exit(EXIT_FAILURE);
    }

    if (exportDT(table2, "output_2a2.csv")) {
        fprintf(stderr, "ERROR: could not open file\n");
        deinitDT(table);
        deinitDT(table2);
        exit(EXIT_FAILURE);
    }

    deinitDT(table);
    deinitDT(table2);

    return 0;
}

Cell_t test(Cell_t cell, Type_t type)
{
    Cell_t ret_cell;

    if (type == DOUBLE) {
        ret_cell.decimal = cell.decimal * 0.0;
    } else if (type == STRING) {
        for (size_t i = 0; i < strlen(cell.string); i++) {
            // note when cell.string[i] = 'Z', 'Z' + 2 = '\\' and this is handled
            // properly.
            ret_cell.string[i] = cell.string[i] + 2;
        }
    } else {
        return cell;
    }

    return ret_cell;
}
