#ifndef CPS1011_TASK2_QB_H_
#define CPS1011_TASK2_QB_H_

#include <stddef.h>

#define STRING_SIZE 64
#define TABLE_ROWS_SIZE 64

extern size_t erow;
extern size_t ecol;

typedef enum {
    ENULL,   // error: null pointer
    EMEM,    // error: memory allocation failed
    EIO,     // error: io operation failed
    ETYPE,   // error: unknown/invalid type detected
    EBOUND,  // error: exceeded some bound (e.g. maximum columns)
    EPARSE,  // error: parsing failed
    EORDER,  // error: parameters of projectDT() must be m <= n and x <= y
} Error_t;

extern Error_t errnoDT;

typedef enum {
    DOUBLE,
    STRING,
} Type_t;

typedef union {
    char *str;
    double dbl;
} Cell_t;

typedef struct {
    char *label;
    Type_t type;
} Field_t;

typedef struct {
    Cell_t **cells;  // [rows][columns]
    Field_t *fields;
    size_t max_cols;  // maximum number of columns
    size_t act_rows;  // active number of rows
    size_t tot_rows;  // total number of allocated rows
} DataTable_t;

/*
 * Operation: initialize an array with the required size.
 *
 * Pre-conditions: enough memory.
 *
 * Parameters: 'fields' is an array, 'size' is the size of the array.
 *
 * Post-condition: the function will return a valid DataTable_t pointer if
 * there is enough memory. Otherwise, a NULL pointer is returned.
 *
 * Possible errors: EMEM
 */
DataTable_t *initDT(Field_t *fields, size_t size);

/*
 * Operation: frees any allocated table.
 *
 * Pre-condition: the dynamically allocated elements within a table
 * must have not been freed. The table itself must have not been freed.
 *
 * Parameters: pointer to DataTable_t struct.
 *
 * Post-condition: all dynamically allocated elements within the table are
 * freed along with the table itself.
 */
void deinitDT(DataTable_t *table);

/*
 * Operation: parse a csv file and populate a DataTable_t struct.
 *
 * Pre-condition: an allocated table and a csv file which follows the bounds
 * (e.g. maximum number of columns) and type specified during initialization.
 *
 * Parameters: an initialized table and a string which refers to a csv file
 * (e.g. "test_data_2a.csv").
 *
 * Post-condition: if the function successfully loads the csv file it will
 * return 0. Otherwise, it will return 1.
 *
 * Possible errors: ENULL, EIO, EMEM, ETYPE, EBOUND, EPARSE
 */
int loadDT(DataTable_t *table, char *csv_file);

/*
 * Operation: the function serializes a DataTable_t into a .csv file.
 *
 * Pre-condition: the table needs to be initialized and the user must have the
 * rights to create a file. (Make sure the file does not already exist because
 * this function is destructive.)
 *
 * Parameters: an initialized table and a string which refers to a csv file
 * (e.g. "test_data_2a.csv").
 *
 * Post-condition: the function will return 0, if writing was successful.
 * Otherwise, the function will return 1.
 *
 * Possible errors: ENULL, EIO
 */
int exportDT(DataTable_t *table, char *csv_file);

/*
 * Operation: this function prints an initialized table in a tabular format.
 * The column width is 10 characters long.
 *
 * Pre-condition: table must be initialized.
 *
 * Parameters: an initialized table.
 *
 * Post-condition: the function will return 0 if successful. Otherwise, the
 * function will return 1.
 *
 * Possible errors: ENULL
 */
int showDT(DataTable_t *table);

/*
 * Operation: this function will return a pointer to a newly allocated subset
 * of an already existing table.
 *
 * Pre-condition: an initialized table, m <= n and x <= y.
 *
 * Parameters: an initialized table, m,n are use to select the columns. x,y are
 * used to select the rows.
 *
 * Post-condition: the function will return a valid pointer if successful.
 * Otherwise, it will return a NULL pointer.
 *
 * Possible errors: ENULL, EORDER, EBOUND, EMEM, ETYPE
 */
DataTable_t *projectDT(DataTable_t *table, size_t m, size_t n, size_t x, size_t y);

/*
 * Operation: this function will aid the user of the library to mutate the
 * contents in a specific column by providing their own function.
 *
 * Pre-condition: the table must be initialized, the column must within be a
 * valid column index within the table. The provided function must follow
 * specified signature.
 *
 * Parameters: an initialized table, a valid column index and a valid function
 * pointer.
 *
 * Post-condition: the function will return a valid pointer to the modified
 * table if successful. Otherwise, it will return a NULL pointer.
 *
 * Possible errors: ENULL, EBOUND
 *
 * N.B.: the user of the library must make sure that the function provided is
 * safe e.g. any resizing of the strings must be handled appropriately.
 */
DataTable_t *mutateDT(DataTable_t *table, size_t col, Cell_t (*fpt)(Cell_t, Type_t));

#endif /* CPS1011_TASK2_QB_H_ */
