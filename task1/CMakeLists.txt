cmake_minimum_required(VERSION 3.10)
project(assignment C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall")

add_executable(question_a1_test src/question_a1_test.c src/question_a.c)
add_executable(question_a2_test src/question_a2_test.c src/question_a.c)
add_executable(question_a3_test src/question_a3_test.c src/question_a.c)
add_executable(question_a4_test src/question_a4_test.c src/question_a.c)
add_executable(question_a5_test src/question_a5_test.c src/question_a.c)
add_executable(question_b src/question_b.c src/question_a.c)
