#include <stddef.h>

#include "question_a.h"

#define MAX_ARRAY_SIZE 200

int main(int argc, char *argv[])
{
    int nums[MAX_ARRAY_SIZE];
    size_t count = init_array(nums, MAX_ARRAY_SIZE);

    val_freq_t list[count];
    size_t count2 = frequency(list, nums, count);

    display_freq(list, count2);

    return 0;
}
