#include <stdio.h>

#include "question_a.h"

#define MAX_ARRAY_SIZE 200

int main(int argc, char *argv[])
{
    int nums[MAX_ARRAY_SIZE];
    int nums_rev[MAX_ARRAY_SIZE];
    size_t count = init_array(nums, MAX_ARRAY_SIZE);

    reverse(nums_rev, nums, count);

    fprintf(stderr, "Output Source: ");

    for (size_t i = 0; i < count; i++) {
        fprintf(stderr, "%d ", nums[i]);
    }

    fprintf(stderr, "\n");

    fprintf(stderr, "Output Destination: ");

    for (size_t i = 0; i < count; i++) {
        fprintf(stderr, "%d ", nums_rev[i]);
    }

    fprintf(stderr, "\n");

    return 0;
}
