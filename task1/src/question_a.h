#ifndef CPS1011_TASK1_QA_H_
#define CPS1011_TASK1_QA_H_

#include <stddef.h>

typedef struct {
    int val;
    size_t freq;
} val_freq_t;

size_t init_array(int *arr, const size_t max_size);
void display(const int *arr, const size_t size);
size_t reverse(int *dest_arr, const int *src_arr, const size_t size);
size_t frequency(val_freq_t *pairs, const int *arr, size_t size);
void display_freq(const val_freq_t *pairs, const size_t size);

#endif /* CPS1011_TASK1_QA_H_ */
