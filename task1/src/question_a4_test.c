#include <stddef.h>
#include <stdio.h>

#include "question_a.h"

#define MAX_ARRAY_SIZE 200

int main(int argc, char *argv[])
{
    int nums[MAX_ARRAY_SIZE];
    size_t count = init_array(nums, MAX_ARRAY_SIZE);

    val_freq_t list[count];
    size_t count2 = frequency(list, nums, count);

    for (size_t i = 0; i < count2; i++) {
        fprintf(stderr, "Value: %d, Frequency: %lu\n", list[i].val, list[i].freq);
    }

    return 0;
}
