#include <stddef.h>
#include <stdio.h>

#include "question_a.h"

#define MAX_ARRAY_SIZE 10

int main(int argc, char *argv[])
{
    int nums[MAX_ARRAY_SIZE];
    size_t count = init_array(nums, MAX_ARRAY_SIZE);

    fprintf(stderr, "Output: ");

    for (size_t i = 0; i < count; i++) {
        fprintf(stderr, "%d ", nums[i]);
    }

    fprintf(stderr, "\n");

    return 0;
}
