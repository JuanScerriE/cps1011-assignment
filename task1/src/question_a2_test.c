#include <stddef.h>

#include "question_a.h"

#define MAX_ARRAY_SIZE 200

int main(int argc, char *argv[])
{
    int nums[MAX_ARRAY_SIZE];
    size_t count = init_array(nums, MAX_ARRAY_SIZE);

    display(nums, count);

    return 0;
}
