#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "question_a.h"

#define MAX_ARR_SIZE 200

int main(void)
{
    int arr[MAX_ARR_SIZE];
    int rev_arr[MAX_ARR_SIZE];
    val_freq_t freq_arr[MAX_ARR_SIZE];

    /*
     * these counts are crucial both for addressing
     * the array and for validation
     */
    size_t count = 0;
    size_t count_rev = 0;
    size_t count_freq = 0;

    unsigned int opt;
    int err;                            // to deal with errors raised by scanf
    int istty = isatty(STDOUT_FILENO);  // is stdout a file or the terminal

    do {
        /*
         * making use of stderr for all printing to tty
         * to avoid prompts being written to stdout
         */
        fprintf(stderr,
                "1) Initialize the array\n2) Display the array\n3) Reverse the array\n4) "
                "Display the reversed array\n5) Compute the frequency of the array\n6) "
                "Display the frequency of the array\n7) Quit\n>> ");

        err = scanf("%u", &opt);

        if (err == 0) {
            fprintf(stderr, "ERROR: invalid conversion\n");
            while (getchar() != '\n')
                ;
            continue;
        }

        if (err < 0) {
            fprintf(stderr, "\nFATAL: EOF or failure before conversion\n");
            exit(EXIT_FAILURE);
        }

        // flush line-buffered stdin
        while (getchar() != '\n')
            ;

        switch (opt) {
            case 1:
                count = init_array(arr, MAX_ARR_SIZE);
                break;
            case 2:
                if (count == 0) {
                    fprintf(stderr, "ERROR: array is empty\n");
                } else {
                    if (istty == 0) {  // use of istty
                        fprintf(stderr, "Writing to stdout\n");
                    }

                    display(arr, count);
                }
                break;
            case 3:
                if (count == 0) {
                    fprintf(stderr, "ERROR: array is empty\n");
                } else {
                    count_rev = reverse(rev_arr, arr, count);
                }
                break;
            case 4:
                if (count_rev == 0) {
                    fprintf(stderr, "ERROR: reversed array is empty\n");
                } else {
                    if (istty == 0) {
                        fprintf(stderr, "Writing to stdout\n");
                    }
                    display(rev_arr, count_rev);
                }
                break;
            case 5:
                if (count == 0) {
                    fprintf(stderr, "ERROR: array is empty\n");
                } else {
                    count_freq = frequency(freq_arr, arr, count);
                }
                break;
            case 6:
                if (count_freq == 0) {
                    fprintf(stderr, "ERROR: value-frequency array is empty\n");
                } else {
                    if (istty == 0) {
                        fprintf(stderr, "Writing to stdout\n");
                    }
                    display_freq(freq_arr, count_freq);
                }
                break;
            case 7:
                fprintf(stderr, "Bye\n");
                break;
            default:
                fprintf(stderr, "ERROR: invalid option\n");
                break;
        }

    } while (opt != 7);

    return 0;
}
