#include "question_a.h"

#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <unistd.h>

size_t init_array(int *arr, const size_t max_size)
{
    size_t count = 0;

    int val;
    char ch;

    // check if stdin is from terminal or through pipping or shell redirection
    if (isatty(STDIN_FILENO))
        fprintf(stderr, "Input a list of integers (%d <= x <= %d) (MAX: %lu):\n", INT_MIN,
                INT_MAX, max_size);

    while ((ch = getchar()) != '\n' && count < max_size) {
        // '-' is considered to make sure that negative integers are catered for
        if (isdigit(ch) || ch == '-') {
            // ungetc places ch back on stdin
            ungetc(ch, stdin);

            // numbers which are larger or smaller than the range will wrap around
            if (scanf("%d", &val) <= 0) {
                fprintf(stderr, "ERROR: Invalid scanf conversion\n");
                break;
            } else {
                arr[count++] = val;
            }
        } else if (!isspace(ch)) {  // numbers are delineated by spaces only
            fprintf(stderr, "ERROR: Invalid list format; list includes: '%c' (%x)\n", ch,
                    ch);
            break;
        }
    }

    if (count == max_size) fprintf(stderr, "ERROR: Maximum reached\n");

    return count;
}

void display(const int *arr, const size_t size)
{
    // printing to stdout by definition of printf
    printf("{\n\t\"array\": [\n");

    if (size > 0) {
        for (size_t i = 0; i < size - 1; i++) {
            printf(
                "\t\t{\n\t\t\t\"offset\": \"%02lu\",\n\t\t\t\"value\": "
                "\"%02d\"\n\t\t},\n\n",
                i, arr[i]);
        }

        printf(
            "\t\t{\n\t\t\t\"offset\": \"%02lu\",\n\t\t\t\"value\": "
            "\"%02d\"\n\t\t}",
            size - 1, arr[size - 1]);
    }

    /*
     * write '\n' on the last line (to adhere
     * to the output of UNIX editors)
     */
    printf("\n\t]\n}\n");
}

size_t reverse(int *dest_arr, const int *src_arr, const size_t size)
{
    for (size_t i = 0, j = size - 1; i < size; i++, j--) {
        dest_arr[i] = src_arr[j];
    }

    return size;
}

size_t frequency(val_freq_t *pairs, const int *arr, size_t size)
{
    size_t count = 0;

    if (size > 0) {
        pairs[count].val = arr[0];
        pairs[count++].freq = 1;
    }

    for (size_t i = 1; i < size; i++) {
        for (size_t j = 0; j < count; j++) {
            if (pairs[j].val == arr[i]) {
                pairs[j].freq++;
                // making use of label to avoid extra variable as flag
                goto CONT;
            }
        }

        // execute if new number
        pairs[count].val = arr[i];
        pairs[count++].freq = 1;
    CONT:;
    }

    return count;
}

void display_freq(const val_freq_t *arr, const size_t size)
{
    printf("{\n\t\"array\": [\n");

    if (size > 0) {
        for (size_t i = 0; i < size - 1; i++) {
            printf(
                "\t\t{\n\t\t\t\"offset\": \"%02lu\",\n\t\t\t\"pair\": {\n"
                "\t\t\t\t\"value\": \"%02d\",\n\t\t\t\t\"freq\": "
                "\"%02lu\"\n\t\t\t}\n\t\t}"
                ",\n\n",
                i, arr[i].val, arr[i].freq);
        }

        printf(
            "\t\t{\n\t\t\t\"offset\": \"%02lu\",\n\t\t\t\"pair\": {\n"
            "\t\t\t\t\"value\": \"%02d\",\n\t\t\t\t\"freq\": \"%02lu\"\n\t\t\t}\n\t\t}",
            size - 1, arr[size - 1].val, arr[size - 1].freq);
    }

    printf("\n\t]\n}\n");
}
